<?php
//establece la hora del servidor 
//modificado en rama1
date_default_timezone_set("Europe/Madrid");

if(!isset($_SESSION)){
	session_start();
}
//incluye el archivo de idioma correspondiente a la variable que esté en sesión sino lo setea a 'es'  incluye el archivo de español
if (isset($_SESSION['idioma'])) {
  switch ($_SESSION['idioma']) {
    case 'es': include ("include/lang/es.php"); break;
    case 'en': include ("include/lang/en.php"); break;
    case 'fr': include ("include/lang/fr.php"); break;
  }
}
else {
	$_SESSION['idioma'] = 'es';
	include ("include/lang/es.php");
}
//si se ha pulsdo el botón envío
if(isset($_POST['envio'])) {
	//si el usuario o la contraseña están vacíos guarda en la session el error y muestra de nuevo la vista
	if(empty($_POST['usuario']) || empty($_POST['password'])) {
		$_SESSION['errorLogin'] = $id['LABEL_VACIO'];
		include ('vistas/loginVista.php');
		include ("vistas/pie.php");
	}
	//sino, guarda los valores en un array y llama a la funcion verificaUsuario que devolverá el tipo de usuario si éste existe o false sino existe
	else{
		$usuario = array();
		$usuario['usuario'] = $_POST['usuario'];
		$usuario['password'] = $_POST['password'];
		include ('modelos/loginModelo.php');
		$modelo = new loginModelo();
		$existe = $modelo->verificaUsuario($usuario);
		//si existe, según el tipo llama al menú correspondiente y guarda el usuario en la sesión
		if ($existe){
			$_SESSION['usuario'] = $usuario['usuario'];
			$_SESSION['errorLogin'] = "";
			if ($existe == "admin") {
				$_SESSION['tipo'] = "admin";
				include ('vistas/menu_adminVista.php');
				include ("vistas/pie.php");
			}
			else if ($existe == "user") {
				$_SESSION['tipo'] = "user";
				include ('vistas/menuVista.php');
				include ("vistas/pie.php");
				echo "<script>swal('".$id['LABEL_BIENVENIDO']." ".$_SESSION['usuario']."', '', 'success');</script>";
			}
		}
		//sino existe guarda en la sesión el error y muestra de nuevo la vista
		else{
			$_SESSION['errorLogin'] = $id['LABEL_NO_COINCIDEN'];
			include ('vistas/loginVista.php');
			include ("vistas/pie.php");
		}
	}
}
//si se ha pulsado el botón registro llama a registro.php
else if (isset($_POST['registro'])) {
	include ('registro.php');
}
//sino se ha pulsado ningun botón, muestra la vista
else{
	include ('vistas/loginVista.php');
	include ("vistas/pie.php");
	
}
?>