<?php
if(!isset($_SESSION)){
	session_start();
}

$_SESSION['tipo'] = 'user';

if (isset($_SESSION['idioma'])) {
  switch ($_SESSION['idioma']) {
    case 'es': include ("include/lang/es.php"); break;
    case 'en': include ("include/lang/en.php"); break;
    case 'fr': include ("include/lang/fr.php"); break;
  }
}
else {
	$_SESSION['idioma'] = 'es';
	include("include/lang/es.php");
}
include ("vistas/registroVista.php");
include ("vistas/pie.php");
?>