<?php
	
	session_start();
	
  require("../../funciones.php");
  
  $conexion = conexion();

  $funcion  = $_POST["funcion"];
  
  $resultado = "[{\"estado\":\"ERROR\"}]";

  if (isset($_SESSION['idioma'])) {
		switch ($_SESSION['idioma']) {
      case 'es': require("../lang/es.php"); break;
      case 'en': require("../lang/en.php"); break;
      case 'fr': require("../lang/fr.php"); break;
    }
  }

	switch ($funcion) {
		case 'comprueba_horario':
			$id = $_POST["id"];
			$date = date('Y-m-d');
			$resultado = "[";

			for ($i=0;$i<7;$i++) {
				$sql = "SELECT * FROM reservas WHERE id_pista = '".$id."' AND f_reserva = '".$date."'";

				if ($sql = mysqli_query($conexion, $sql)) {
					if (!$row = mysqli_fetch_array($sql)) {
						$sql2 = "INSERT INTO reservas (id_pista, f_reserva) VALUES ('".$id."', '".$date."')";
						mysqli_query($conexion, $sql2);
					}
				}
				$date = strtotime('+1 day', strtotime($date));
				$date = date ('Y-m-j',$date);
			}
			$resultado .= "{\"estado\":\"OK\"}]";
			break;

		case 'muestra_horario':
			$pista = $_POST["pista"];
			$date = date('Y-m-d');
			$dia = date ('l');
			$hora = date ('H:i');
			$resultado = "[";



			for ($i=0;$i<7;$i++) {
				$sql = "SELECT * FROM reservas WHERE id_pista = '".$pista."' AND f_reserva = '".$date."'";

				switch ($dia) {
					case 'Monday': $dia = $id['LABEL_LUNES'];break;
					case 'Tuesday': $dia = $id['LABEL_MARTES'];break;
					case 'Wednesday': $dia = $id['LABEL_MIERCOLES'];break;
					case 'Thursday': $dia = $id['LABEL_JUEVES'];break;
					case 'Friday': $dia = $id['LABEL_VIERNES'];break;
					case 'Saturday': $dia = $id['LABEL_SABADO'];break;
					case 'Sunday': $dia = $id['LABEL_DOMINGO'];break;
				}
				if ($sql = mysqli_query($conexion, $sql)) {
					if ($row = mysqli_fetch_array($sql)) {
						$resultado .= "{\"fecha\":\"".$dia." ".$row['f_reserva']."\",";

						if ($row['hora01'] != '')
							$resultado .= "\"h1\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h1\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "09:00")
								$resultado .= "\"reservable1\":\"no\",";
							else
								$resultado .= "\"reservable1\":\"si\",";
						}
						if ($row['hora02'] != '')
							$resultado .= "\"h2\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h2\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "10:00")
								$resultado .= "\"reservable2\":\"no\",";
							else
								$resultado .= "\"reservable2\":\"si\",";
						}
						if ($row['hora03'] != '')
							$resultado .= "\"h3\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h3\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "11:00")
								$resultado .= "\"reservable3\":\"no\",";
							else
								$resultado .= "\"reservable3\":\"si\",";
						}
						if ($row['hora04'] != '')
							$resultado .= "\"h4\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h4\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "12:00")
								$resultado .= "\"reservable4\":\"no\",";
							else
								$resultado .= "\"reservable4\":\"si\",";
						}
						if ($row['hora05'] != '')
							$resultado .= "\"h5\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h5\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "13:00")
								$resultado .= "\"reservable5\":\"no\",";
							else
								$resultado .= "\"reservable5\":\"si\",";
						}
						if ($row['hora06'] != '') 
							$resultado .= "\"h6\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h6\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "17:00")
								$resultado .= "\"reservable6\":\"no\",";
							else
								$resultado .= "\"reservable6\":\"si\",";
						}
						if ($row['hora07'] != '')
							$resultado .= "\"h7\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h7\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "18:00")
								$resultado .= "\"reservable7\":\"no\",";
							else
								$resultado .= "\"reservable7\":\"si\",";
						}
						if ($row['hora08'] != '')
							$resultado .= "\"h8\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h8\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "19:00")
								$resultado .= "\"reservable8\":\"no\",";
							else
								$resultado .= "\"reservable8\":\"si\",";
						}
						if ($row['hora09'] != '')
							$resultado .= "\"h9\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h9\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "20:00")
								$resultado .= "\"reservable9\":\"no\",";
							else
								$resultado .= "\"reservable9\":\"si\",";
						}
						if ($row['hora10'] != '')
							$resultado .= "\"h10\":\"".$id['LABEL_OCUPADA']."\",";
						else {
							$resultado .= "\"h10\":\"".$id['LABEL_LIBRE']."\",";
							if ($date == date('Y-m-d') AND $hora > "21:00")
								$resultado .= "\"reservable10\":\"no\",";
							else
								$resultado .= "\"reservable10\":\"si\",";
						}

						if ($i == 6 ) 
							$resultado .= "\"estado\":\"OK\"}";
						else
							$resultado .= "\"estado\":\"OK\"},";
					}
				}
				$date = strtotime('+1 day', strtotime($date));
				$dia = date ('l', $date);
				$date = date ('Y-m-j',$date);
				
			}
			$resultado .= "]";
			break;
		
		case 'reserva_hora':
			$dia = split (' ', $_POST["dia"]);
			$hora = $_POST["hora"];
			$pista = $_POST["pista"];
			$usuario = $_SESSION["usuario"];

			switch ($hora) {
				case '9:00': $hora_r = 'hora01';break;
				case '10:00': $hora_r = 'hora02';break;
				case '11:00': $hora_r = 'hora03';break;
				case '12:00': $hora_r = 'hora04';break;
				case '13:00': $hora_r = 'hora05';break;
				case '17:00': $hora_r = 'hora06';break;
				case '18:00': $hora_r = 'hora07';break;
				case '19:00': $hora_r = 'hora08';break;
				case '20:00': $hora_r = 'hora09';break;
				case '21:00': $hora_r = 'hora10';break;
			}

			$sql = "UPDATE reservas SET $hora_r = '$usuario' WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'";

			if ($sql = mysqli_query($conexion, $sql)) {
				$resultado = "[{\"estado\":\"".$sql."\"}]";
			}

			break;
		case 'comprobar_libre':
			$dia = $_POST["dia"];
			$dia = preg_split('/ /', $dia);
			$hora = $_POST["hora"];
			$pista = $_POST["pista"];

			switch ($hora) {
				case '9:00': $sql = "SELECT hora01 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '10:00': $sql = "SELECT hora02 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '11:00': $sql = "SELECT hora03 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '12:00': $sql = "SELECT hora04 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '13:00': $sql = "SELECT hora05 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '17:00': $sql = "SELECT hora06 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '18:00': $sql = "SELECT hora07 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '19:00': $sql = "SELECT hora08 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '20:00': $sql = "SELECT hora09 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
				case '21:00': $sql = "SELECT hora10 AS hora FROM reservas WHERE f_reserva = '$dia[1]' AND id_pista = '$pista'"; break;
			}

			

			if ($sql = mysqli_query($conexion, $sql)) {
				if ($row = mysqli_fetch_array($sql)) {
					$resultado = "[{\"pista\":\"".$row['hora']."\",\"estado\":\"OK\"}]";
				}
			}
			//$resultado = "[{\"pista\":\"".$pista."\",\"estado\":\"OK\"}]";
			break;
		case 'cambiar_idioma':
			$idioma = $_POST["id"];
			$pista = $_POST["pista"];
			$_SESSION['idioma'] = $idioma;
			$resultado = "[{\"estado\":\"OK\"}]";
			break;
	}
//$resultado = "[{\"1\":\"hora1\"}]";
	echo $resultado;exit;
?>