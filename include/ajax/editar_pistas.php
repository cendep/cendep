<?php
	
	session_start();
	
  require("../../funciones.php");
  
  $conexion = conexion();

  $funcion  = $_POST["funcion"];
  
  $resultado = "[{\"estado\":\"ERROR\"}]";

	switch ($funcion) {
		case 'cargar_datos':
			$id = $_POST['id'];

			$sql = "SELECT * FROM pistas WHERE id_pista = '".$id."'";

			if ($sql = mysqli_query($conexion, $sql)) {
				if ($row = mysqli_fetch_array($sql)) {
					$resultado = "[{\"nom_es\":\"".$row['nom_pista_es']."\",";
					$resultado .= "\"nom_en\":\"".$row['nom_pista_en']."\",";
					$resultado .= "\"nom_fr\":\"".$row['nom_pista_fr']."\",";
					$resultado .= "\"estado\":\"OK\"}]";
				}
			}
			break;
		case 'comprueba_nombres':
      if (isset($_POST['id'])) {
        $id = $_POST['id'];
        $and = "AND id_pista <> '".$id."'";
      }
      else {
        $and = '';
      }
			$nom_es = $_POST["nom_es"];
      $nom_en = $_POST["nom_en"];
      $nom_fr = $_POST["nom_fr"];

      $resultado = "[";

      $sql = "SELECT * FROM pistas WHERE nom_pista_es = '".$nom_es."' $and";
      $sql2 = "SELECT * FROM pistas WHERE nom_pista_en = '".$nom_en."' $and";
      $sql3 = "SELECT * FROM pistas WHERE nom_pista_fr = '".$nom_fr."' $and";
      $encontrado = false;

      if ($sql = mysqli_query($conexion, $sql)) {
        $sql2 = mysqli_query($conexion, $sql2);
        $sql3 = mysqli_query($conexion, $sql3);
        if (mysqli_fetch_array($sql)) {
          $resultado .= "{\"repetido\":\"nom_es\",";
          $resultado .= "\"estado\":\"ERROR\"}";
          $encontrado = true;
        }
        else if (mysqli_fetch_array($sql2)) {
          $resultado .= "{\"repetido\":\"nom_en\",";
          $resultado .= "\"estado\":\"ERROR\"}";
          $encontrado = true;
        }
        else if (mysqli_fetch_array($sql3)) {
          $resultado .= "{\"repetido\":\"nom_fr\",";
          $resultado .= "\"estado\":\"ERROR\"}";
          $encontrado = true;
        }
      }
      if ($encontrado)
        $resultado .= "]";
      else
        $resultado .= "{\"estado\":\"OK\"}]";
      break;
    case 'registra_pista':
      $nom_es = $_POST["nom_es"];
      $nom_en = $_POST["nom_en"];
      $nom_fr = $_POST["nom_fr"];

      $sql = "INSERT INTO pistas (nom_pista_es, nom_pista_en, nom_pista_fr) 
                VALUES ('".$nom_es."', '".$nom_en."', '".$nom_fr."')";

      if ($sql = mysqli_query($conexion, $sql)) {
        $resultado = "[{\"estado\":\"OK\"}]";
      }
      break;
		case 'editar_pista':
			$id = $_POST['id'];
			$nom_es = $_POST['nom_es'];
			$nom_en = $_POST['nom_en'];
			$nom_fr = $_POST['nom_fr'];

			$sql = "UPDATE pistas SET nom_pista_es = '".$nom_es."', nom_pista_en = '".$nom_en."', nom_pista_fr = '".$nom_fr."'
							WHERE id_pista = '".$id."'";
			if ($sql = mysqli_query($conexion, $sql)) {
				$resultado = "[{\"estado\":\"OK\"}]";
			}
			break;
    case 'guardar_imagen':
      $id = $_POST['id'];
      $imagen = $_POST['imagen'];

      $sql= "SELECT * FROM pistas WHERE id_pista = '".$id."'";
      if ($sql = mysqli_query($conexion, $sql)) {
        if ($row = mysqli_fetch_array($sql)) {
          $imagen_antigua = $row['imagen_pista'];
          if ($imagen == $imagen_antigua) {
            $resultado = "[{\"motivo\":\"iguales\",";
            $resultado .= "\"estado\":\"ERROR\"}]";
          }
          else {
            if (file_exists('../img/pistas/files/'.$imagen_antigua)) {
            unlink('../img/pistas/files/'.$imagen_antigua);
            unlink('../img/pistas/files/thumbnail/'.$imagen_antigua);
            }
          }
        }
      }


      $sql = "UPDATE pistas SET imagen_pista = '".$imagen."' WHERE id_pista = '".$id."'";

      if ($sql = mysqli_query($conexion, $sql)) {
        $resultado = "[{\"estado\":\"OK\"}]";
      }

      break;
    case 'cambiar_idioma':
      $idioma = $_POST["id"];
      $_SESSION['idioma'] = $idioma;
      $resultado = "[{\"estado\":\"OK\"}]";
      break;
	}
	echo $resultado;exit;
?>