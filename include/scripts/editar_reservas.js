window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/editar_reservas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_reservas.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/editar_reservas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_reservas.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/editar_reservas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_reservas.php?id=fr');
      }
    });
  });
  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }
  $("#flex2").attr('unselectable', 'on')
              .css('user-select', 'none')
              .on('selectstart', false);

  $("#flex2").flexigrid({
    url: 'include/json/reservas.php',
    dataType: 'json',
    colModel: [
      {display: 'Id', name: 'id_reserva', width: 30, sortable: true, align: 'left'},
      {display: 'Pista', name: 'nom_pista_es', width: 120, sortable: true, align: 'left', searchable: true},
      {display: 'Fecha', name: 'f_reserva', width: 70, sortable: true, align: 'left', searchable: true},
      {display: '9:00-10:00', name: 'hora01', width: 70, sortable: false, align: 'left'},
      {display: '10:00-11:00', name: 'hora02', width: 70, sortable: false, align: 'left'},
      {display: '11:00-12:00', name: 'hora03', width: 70, sortable: false, align: 'left'},
      {display: '12:00-13:00', name: 'hora04', width: 70, sortable: false, align: 'left'},
      {display: '13:00-14:00', name: 'hora05', width: 70, sortable: false, align: 'left'},
      {display: '17:00-18:00', name: 'hora06', width: 70, sortable: false, align: 'left'},
      {display: '18:00-19:00', name: 'hora07', width: 70, sortable: false, align: 'left'},
      {display: '19:00-20:00', name: 'hora08', width: 70, sortable: false, align: 'left'},
      {display: '20:00-21:00', name: 'hora09', width: 70, sortable: false, align: 'left'},
      {display: '21:00-22:00', name: 'hora10', width: 70, sortable: false, align: 'left'}
    ],
    buttons: [
        {name: 'Añadir', bclass: 'add', onpress: test},
        {separator: true},  
        {name: 'Editar', bclass: 'edit', onpress: test},
        {separator: true}
    ],
    searchitems: [
        {display: 'Pista', name: 'nom_pista_es', isdefault: true},
        {display: 'Fecha', name: 'f_reserva'}
    ],
    sortname: "id_reserva",
    sortorder: "asc",
    usepager: true,
    rp: 10,
    showTableToggleBtn: true, //PARA ESCONDER O NO EL GRID
    singleSelect: true,
    height: 240,
    searchBar: true
  });  

  function test() {

  }
}