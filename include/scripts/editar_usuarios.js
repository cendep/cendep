window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/editar_usuarios.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_usuarios.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/editar_usuarios.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_usuarios.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/editar_usuarios.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_usuarios.php?id=fr');
      }
    });
  });
  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }

  $("#flex1").attr('unselectable', 'on')
              .css('user-select', 'none')
              .on('selectstart', false);

  setTimeout(function(){ 
    $("#flex1").flexigrid({
      url: 'include/json/usuarios.php',
      dataType: 'json',
      colModel: [
        {display: 'Id', name: 'id_usuario', width: 65, sortable: true, align: 'left'},
        {display: id['LABEL_NOMBRE'], name: 'nombre', width: 95, sortable: true, align: 'left'},
        {display: id['LABEL_CORREO'], name: 'correo', width: 200, sortable: false, align: 'left'},
        {display: id['LABEL_TELEFONO'], name: 'telefono', width: 80, sortable: false, align: 'left'}
      ],
      buttons: [
          {name: id['LABEL_B_ANADIR'], bclass: 'add', onpress: test},
          {separator: true},  
          {name: id['LABEL_B_EDITAR'], bclass: 'edit', onpress: test},
          {separator: true}
      ],
      searchitems: [
          {display: 'Id', name: 'id_usuario', isdefault: true},
          {display: id['LABEL_NOMBRE'], name: 'nombre'}
      ],
      sortname: "id_usuario",
      sortorder: "asc",
      usepager: true,
      showTableToggleBtn: false, //PARA ESCONDER O NO EL GRID
      singleSelect: true
    }); 
  }, 1000); 

  $('form').submit(function (event) {
    event.preventDefault();
  });

  setTimeout(function(){  
    $("#formAnadir").validate({
      rules: {
        usuario: { 
          required: true,
          minlength: 6
        },
        password: {
          required: true,
          minlength: 6 
        },
        password2: {
          required: true,
          minlength: 6,
          equalTo: '#contrasena'
        },
        correo: {
          required: true,
          email: true
        },
        telf: {
          required: true,
          digits: true,
          minlength: 9,
          maxlength: 9
        }
      },
      messages: {
        usuario: {
          required: id['LABEL_INTR_USUARIO'],
          minlength: id['LABEL_INTR_USUARIO2']
        },
        password: {
          required: id['LABEL_INTR_CONTRASENA'],
          minlength: id['LABEL_INTR_CONTRASENA3']
        },
        password2: {
          required: id['LABEL_INTR_CONTRASENA2'],
          minlength: id['LABEL_INTR_CONTRASENA3'],
          equalTo: id['LABEL_INTR_CONTRASENA4']
        },
        correo: {
          required: id['LABEL_INTR_CORREO'],
          email: id['LABEL_INTR_CORREO2']
        },
        telf: {
          required: id['LABEL_INTR_TELEFONO'],
          minlength: id['LABEL_INTR_TELEFONO2'],
          maxlength: id['LABEL_INTR_TELEFONO2'],
          digits: id['LABEL_INTR_TELEFONO3']
        }
      },
      submitHandler: function(form) {
        enviarRegistro();
      }
    });//validate
    $("#formEditar").validate({
      rules: {
        e_usuario: { 
          required: true,
          minlength: 6
        },
        e_correo: {
          required: true,
          email: true
        },
        e_telf: {
          required: true,
          digits: true,
          minlength: 9,
          maxlength: 9
        }
      },
      messages: {
        e_usuario: {
          required: id['LABEL_INTR_USUARIO'],
          minlength: id['LABEL_INTR_USUARIO2']
        },
        e_correo: {
          required: id['LABEL_INTR_CORREO'],
          email: id['LABEL_INTR_CORREO2']
        },
        e_telf: {
          required: id['LABEL_INTR_TELEFONO'],
          minlength: id['LABEL_INTR_TELEFONO2'],
          maxlength: id['LABEL_INTR_TELEFONO2'],
          digits: id['LABEL_INTR_TELEFONO3']
        }
      },
      submitHandler: function(form) {
        enviarEditar();
      }
    });//validate
  }, 1000);

  function test(opcion, grid) {
    if (opcion == "Añadir") {
      $('#Anadir').trigger('click');
    }
    else if (opcion == "Editar") {
      if ($('.trSelected').length != 0) {
        $.ajax({
          url: "./include/ajax/editar_usuarios.php",
          type: "POST",
          data: {
            funcion: "cargar_datos",
            id: $('.trSelected').attr('data-id')
          },
          success: function(datos){
            console.log(datos)
            var data = JSON.parse(datos);
            if (data[0].estado == "OK") {
              $('#e_usuario').val(data[0].nombre);
              $('#e_correo').val(data[0].correo);
              $('#e_telf').val(data[0].telefono);
            }
          }
        });
        $('#Editar').trigger('click');
      }
      else {
        swal ("Error", "Debe seleccionar una fila", "info"); 
      }
    }

  }//test

  function enviarRegistro() {
    var usuario = $("#usuario").val();
    var contrasena = $("#contrasena").val();
    var correo = $("#correo").val();
    var telf = $("#telf").val();
    var tipo = $("#tipo").val();

    $.ajax({
      url: "./include/ajax/registro.php",
      type: "POST",
      data: {
        funcion: "comprueba_usuario",
        usuario: usuario,
        correo: correo
      },
      success: function (datos) {
        var data = JSON.parse(datos);
        if (data[0].estado == 'ERROR') {
          if (data[0].repetido == 'usuario')
            swal ("Error", id['LABEL_EXISTE_USU'], "error"); 
          else
            swal ("Error", id['LABEL_EXISTE_CORREO'], "error"); 
        }
        else {
          $.ajax({
            url: "./include/ajax/registro.php",
            type: "POST",
            data: {
              funcion: "registra_usuario",
              usuario: usuario,
              contrasena: contrasena,
              correo: correo,
              telf: telf,
              tipo: tipo
            },
            success: function (datos) {
              var data = JSON.parse(datos);
              
              if (data[0].estado == 'OK') {
                swal (id['LABEL_REGISTRADO'], id['LABEL_REGISTRADO3'], "success");
                setTimeout(function(){ 
                  $(location).attr('href','editar_usuarios.php');
                }, 3000);
              }
            }
          });//ajax
        }
      }
    });//ajax 
  };// enviarRegistro

  function enviarEditar() {
    var usuario = $("#e_usuario").val();
    var correo = $("#e_correo").val();
    var telf = $("#e_telf").val();

    $.ajax({
      url: "./include/ajax/editar_usuarios.php",
      type: "POST",
      data: {
        funcion: "comprueba_usuario",
        id: $('.trSelected').attr('data-id'),
        usuario: usuario,
        correo: correo
      },
      success: function (datos) {
        var data = JSON.parse(datos);
        if (data[0].estado == 'ERROR') {
          if (data[0].repetido == 'usuario')
            swal ("Error", id['LABEL_EXISTE_USU'], "error"); 
          else
            swal ("Error", id['LABEL_EXISTE_CORREO'], "error"); 
        }
        else {
          $.ajax({
            url: "./include/ajax/editar_usuarios.php",
            type: "POST",
            data: {
              funcion: "editar_usuario",
              id: $('.trSelected').attr('data-id'),
              usuario: usuario,
              correo: correo,
              telf: telf
            },
            success: function (datos) {
              var data = JSON.parse(datos);
              
              if (data[0].estado == 'OK') {
                swal (id['LABEL_REGISTRADO'], id['LABEL_REGISTRADO3'], "success");
                setTimeout(function(){ 
                  $(location).attr('href','editar_usuarios.php');
                }, 3000);
              }
            }
          });//ajax
        }
      }
    });//ajax 
  };// enviarRegistro
}