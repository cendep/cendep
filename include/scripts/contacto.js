window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/contacto.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'contacto.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/contacto.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'contacto.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/contacto.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'contacto.php?id=fr');
      }
    });
  });

  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }

  setTimeout(function(){ 
    $("#formContacto").validate({
      rules: {
        asunto: { 
          required: true
        },
        correo: {
          required: true,
          email: true
        },
        mensaje: {
          required: true
        }
      },
      messages: {
        asunto: {
          required: id['LABEL_INTR_ASUNTO']
        },
        correo: {
          required: id['LABEL_INTR_CORREO'],
          email: id['LABEL_INTR_CORREO2']
        },
        mensaje: {
          required: id['LABEL_INTR_MENSAJE']
        }
      },
      submitHandler: function(form) {
        enviarCorreo();
      }
    });//validate
  }, 1000);

  function enviarCorreo() {
    swal({
      title: id['LABEL_SEGURO'],
      text: id['LABEL_SEGURO2']+$('#mensaje').val(),
      type: "info",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      confirmButtonText: id['LABEL_ENVIALO'],
      cancelButtonText: "No",
    },
    function(){
      $.ajax({
      url: "./include/ajax/contacto.php",
      type: "POST",
      data: {
        funcion: "enviar_correo",
        asunto: $('#asunto').val(),
        correo: $('#correo').val(),
        mensaje: $('#mensaje').val()
      },
      success: function (datos) {
        json = JSON.parse(datos);
        if (json[0].estado != 'ERROR') {
          swal(id['LABEL_ENVIADO'], id['LABEL_ENVIADO2'], "success");
          $('#asunto').val('');
          $('#correo').val('');
          $('#mensaje').val('');
        }
      }
    });
    });
    
  }
}