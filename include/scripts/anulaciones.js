window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/anulaciones.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'anulaciones.php?id=es');
      }
    });
  });

$("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/anulaciones.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'anulaciones.php?id=en');
      }
    });
  });

$("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/anulaciones.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'anulaciones.php?id=fr');
      }
    });
  });

  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }
  
  mostrarReservas();

  function mostrarReservas() {
    $.ajax({
      url: "./include/ajax/anulaciones.php",
      type: "POST",
      data: {
        funcion: "mostrar_reservas"
      },
      success: function (datos) {
        var data = JSON.parse(datos);
        
        for (var i=0;i<data.length; i++) {
          if (data[i].h1) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora01'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>9:00-10:00</td></tr>")
          }
          if (data[i].h2) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora02'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>10:00-11:00</td></tr>")
          }
          if (data[i].h3) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora03'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>11:00-12:00</td></tr>")
          }
          if (data[i].h4) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora04'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>12:00-13:00</td></tr>")
          }
          if (data[i].h5) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora05'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>13:00-14:00</td></tr>")
          }
          if (data[i].h6) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora06'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>17:00-18:00</td></tr>")
          }
          if (data[i].h7) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora07'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>18:00-19:00</td></tr>")
          }
          if (data[i].h8) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora08'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>19:00-20:00</td></tr>")
          }
          if (data[i].h9) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora09'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>20:00-21:00</td></tr>")
          }
          if (data[i].h10) {
            $("#t_reservas").append("<tr id='"+data[i].fecha+"/"+data[i].pista+"/hora10'class='elim warning'><td>"+data[i].fecha+"</td><td>"+data[i].pista_nom+"</td><td>21:00-22:00</td></tr>")
          }
        }
        $('.elim'). hover ( 
          function() {
            $(this).css({"border":"solid red"});
          },
          function() {
            $(this).css({"border":"solid 1px #ddd"});
          }
        );
        $('.elim').click(function() {
          var datos = $(this).attr('id').split("/");
          var fecha = datos[0];
          var pista = datos[1];
          var hora = datos[2];

          cancelarHora(fecha, pista, hora);
        });
      }
    });
  };

  function cancelarHora(fecha, pista, hora) {
    swal({
      title: id['LABEL_SEGURO'],
      text: id['LABEL_CANCELARA']+" "+pista+" "+id['LABEL_CANCELARA2']+fecha+" ",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: id['LABEL_CONFIRMAR'],
      closeOnConfirm: false
    }, function(){
      $.ajax({
        url: "./include/ajax/anulaciones.php",
        type: "POST",
        data: {
          funcion: "cancela_hora",
          dia: fecha,
          hora: hora,
          pista: pista
        },
        success: function (datos) {
          swal(id['LABEL_CANCELADO'], id['LABEL_CANCELADO2']+" "+pista+" "+id['LABEL_CANCELADO3']+fecha, "success");
          $("tr.elim").remove();
          mostrarReservas();
        }
      });
    });
  };
};