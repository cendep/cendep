window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/registro.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'registro.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/registro.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'registro.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/registro.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'registro.php?id=fr');
      }
    });
  });

  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }
   
  $('form').submit(function (event) {
    event.preventDefault();
  });
  
  setTimeout(function(){  
    $("#formRegistro").validate({
      rules: {
        usuario: { 
          required: true,
          minlength: 6
        },
        password: {
          required: true,
          minlength: 6 
        },
        password2: {
          required: true,
          minlength: 6,
          equalTo: '#contrasena'
        },
        correo: {
          required: true,
          email: true
        },
        telf: {
          required: true,
          digits: true,
          minlength: 9,
          maxlength: 9
        }
      },
      messages: {
        usuario: {
          required: id['LABEL_INTR_USUARIO'],
          minlength: id['LABEL_INTR_USUARIO2']
        },
        password: {
          required: id['LABEL_INTR_CONTRASENA'],
          minlength: id['LABEL_INTR_CONTRASENA3']
        },
        password2: {
          required: id['LABEL_INTR_CONTRASENA2'],
          minlength: id['LABEL_INTR_CONTRASENA3'],
          equalTo: id['LABEL_INTR_CONTRASENA4']
        },
        correo: {
          required: id['LABEL_INTR_CORREO'],
          email: id['LABEL_INTR_CORREO2']
        },
        telf: {
          required: id['LABEL_INTR_TELEFONO'],
          minlength: id['LABEL_INTR_TELEFONO2'],
          maxlength: id['LABEL_INTR_TELEFONO2'],
          digits: id['LABEL_INTR_TELEFONO3']
        }
      },
      submitHandler: function(form) {
        enviarRegistro();
      }
    });//validate
  }, 1000);
  
  function enviarRegistro() {
    var usuario = $("#usuario").val();
    var contrasena = $("#contrasena").val();
    var correo = $("#correo").val();
    var telf = $("#telf").val();

    $.ajax({
      url: "./include/ajax/registro.php",
      type: "POST",
      data: {
        funcion: "comprueba_usuario",
        usuario: usuario,
        correo: correo
      },
      success: function (datos) {
        var data = JSON.parse(datos);
        if (data[0].estado == 'ERROR') {
          if (data[0].repetido == 'usuario')
            swal ("Error", id['LABEL_EXISTE_USU'], "error"); 
          else
            swal ("Error", id['LABEL_EXISTE_CORREO'], "error"); 
        }
        else {
          $.ajax({
            url: "./include/ajax/registro.php",
            type: "POST",
            data: {
              funcion: "registra_usuario",
              usuario: usuario,
              contrasena: contrasena,
              correo: correo,
              telf: telf
            },
            success: function (datos) {
              var data = JSON.parse(datos);
              
              if (data[0].estado == 'OK') {
                swal (id['LABEL_REGISTRADO'], id['LABEL_REGISTRADO2'], "success");
                setTimeout(function(){ 
                  $(location).attr('href','menu.php');
                }, 3000);
              }
            }
          });//ajax
        }
      }
    });//ajax 
  };
};