window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/editar_pistas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_pistas.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/editar_pistas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_pistas.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/editar_pistas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_pistas.php?id=fr');
      }
    });
  });
  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }
  $("#flex3").attr('unselectable', 'on')
              .css('user-select', 'none')
              .on('selectstart', false);

  setTimeout(function(){ 
    $("#flex3").flexigrid({
      url: 'include/json/pistas.php',
      dataType: 'json',
      colModel: [
        {display: 'Id', name: 'id_pista', width: 65, sortable: true, align: 'left'},
        {display: id['LABEL_NOM_ES'], name: 'nom_pista_es', width: 150, sortable: false, align: 'left'},
        {display: id['LABEL_NOM_EN'], name: 'nom_pista_en', width: 150, sortable: false, align: 'left'},
        {display: id['LABEL_NOM_FR'], name: 'nom_pista_fr', width: 150, sortable: false, align: 'left'},
        {display: id['LABEL_IMAGEN'], name: 'imagen', width: 150, sortable: false, align: 'left'}
      ],
      buttons: [
          {name: id['LABEL_B_ANADIR'], bclass: 'add', onpress: test},
          {separator: true},  
          {name: id['LABEL_B_EDITAR'], bclass: 'edit', onpress: test},
          {separator: true},
          {name: id['LABEL_B_FOTO'], bclass: 'edit', onpress: test},
          {separator: true}
      ],
      searchitems: [
          {display: 'Id', name: 'id_pista', isdefault: true},
          {display: id['LABEL_NOM_ES'], name: 'nom_pista_es'},
          {display: id['LABEL_NOM_EN'], name: 'nom_pista_en'},
          {display: id['LABEL_NOM_FR'], name: 'nom_pista_fr'}
      ],
      sortname: "id_pista",
      sortorder: "asc",
      usepager: true,
      showTableToggleBtn: false, //PARA ESCONDER O NO EL GRID
      singleSelect: true
    });  
  }, 1000);

  $('form').submit(function (event) {
    event.preventDefault();
  });

  setTimeout(function(){  
    $("#formAnadir").validate({
      rules: {
        nom_es: { 
          required: true
        },
        nom_en: {
          required: true
        },
        nom_fr: {
          required: true
        }
      },
      messages: {
        nom_es: {
          required: id['LABEL_INTR_NOM_ES']
        },
        nom_en: {
          required: id['LABEL_INTR_NOM_EN']
        },
        nom_fr: {
          required: id['LABEL_INTR_NOM_FR']
        }
      },
      submitHandler: function(form) {
        anadirPista();
      }
    });//validate
    $("#formEditar").validate({
      rules: {
        e_nom_es: { 
          required: true
        },
        e_nom_en: {
          required: true
        },
        e_nom_fr: {
          required: true
        }
      },
      messages: {
        e_nom_es: {
          required: id['LABEL_INTR_NOM_ES']
        },
        e_nom_en: {
          required: id['LABEL_INTR_NOM_EN']
        },
        e_nom_fr: {
          required: id['LABEL_INTR_NOM_FR']
        }
      },
      submitHandler: function(form) {
        editarPista();
      }
    });//validate
  }, 1000);

  

  function test(opcion, grid) {
    if (opcion == "Añadir") {
      $('#Anadir').trigger('click');
    }
    else if (opcion == "Editar") {
      if ($('.trSelected').length != 0) {
        $.ajax({
          url: "./include/ajax/editar_pistas.php",
          type: "POST",
          data: {
            funcion: "cargar_datos",
            id: $('.trSelected').attr('data-id')
          },
          success: function(datos){
            var data = JSON.parse(datos);
            if (data[0].estado == "OK") {
              $('#e_nom_es').val(data[0].nom_es);
              $('#e_nom_en').val(data[0].nom_en);
              $('#e_nom_fr').val(data[0].nom_fr);
            }
          }
        });
        $('#Editar').trigger('click');
      }
      else {
        swal ("Error", "Debe seleccionar una fila", "info"); 
      }
    }
    else if (opcion == "Subir Foto") {
      if ($('.trSelected').length != 0) {
        $('#SubirFoto').trigger('click');
      }
      else {
        swal ("Error", "Debe seleccionar una fila", "info"); 
      }
    }

  }//test

  function anadirPista() {
    var nom_es = $("#nom_es").val();
    var nom_en = $("#nom_en").val();
    var nom_fr = $("#nom_fr").val();

    $.ajax({
      url: "./include/ajax/editar_pistas.php",
      type: "POST",
      data: {
        funcion: "comprueba_nombres",
        nom_es: nom_es,
        nom_en: nom_en,
        nom_fr: nom_fr
      },
      success: function (datos) {
        console.log(datos)
        var data = JSON.parse(datos);
        if (data[0].estado == 'ERROR') {
          if (data[0].repetido == 'nom_es')
            swal ("Error", id['LABEL_EXISTE_NOM_ES'], "error"); 
          else if (data[0].repetido == 'nom_en')
            swal ("Error", id['LABEL_EXISTE_NOM_EN'], "error"); 
          else
            swal ("Error", id['LABEL_EXISTE_NOM_FR'], "error"); 
        }
        else {
          $.ajax({
            url: "./include/ajax/editar_pistas.php",
            type: "POST",
            data: {
              funcion: "registra_pista",
              nom_es: nom_es,
              nom_en: nom_en,
              nom_fr: nom_fr
            },
            success: function (datos) {
              var data = JSON.parse(datos);
              
              if (data[0].estado == 'OK') {
                swal (id['LABEL_REGISTRADA'], id['LABEL_REGISTRADA2'], "success");
                setTimeout(function(){ 
                  $(location).attr('href','editar_pistas.php');
                }, 2500);
              }
            }
          });//ajax
        }
      }
    });//ajax 
  };// anadirPista

  function editarPista() {
    var nom_es = $("#e_nom_es").val();
    var nom_en = $("#e_nom_en").val();
    var nom_fr = $("#e_nom_fr").val();

    $.ajax({
      url: "./include/ajax/editar_pistas.php",
      type: "POST",
      data: {
        funcion: "comprueba_nombres",
        id: $('.trSelected').attr('data-id'),
        nom_es: nom_es,
        nom_en: nom_en,
        nom_fr: nom_fr
      },
      success: function (datos) {
        var data = JSON.parse(datos);
        if (data[0].estado == 'ERROR') {
          if (data[0].repetido == 'nom_es')
            swal ("Error", id['LABEL_EXISTE_NOM_ES'], "error"); 
          else if (data[0].repetido == 'nom_en')
            swal ("Error", id['LABEL_EXISTE_NOM_EN'], "error"); 
          else if (data[0].repetido == 'nom_fr')
            swal ("Error", id['LABEL_EXISTE_NOM_FR'], "error"); 
        }
        else {
          $.ajax({
            url: "./include/ajax/editar_pistas.php",
            type: "POST",
            data: {
              funcion: "editar_pista",
              id: $('.trSelected').attr('data-id'),
              nom_es: nom_es,
              nom_en: nom_en,
              nom_fr: nom_fr
            },
            success: function (datos) {
              var data = JSON.parse(datos);
              
              if (data[0].estado == 'OK') {
                swal (id['LABEL_CAMBIADA'], id['LABEL_CAMBIADA2'], "success");
                setTimeout(function(){ 
                  $(location).attr('href','editar_pistas.php');
                }, 2500);
              }
            }
          });//ajax
        }
      }
    });//ajax 
  };// enviarEditar
}