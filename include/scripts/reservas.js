window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/reservas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'reservas.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/reservas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'reservas.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/reservas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'reservas.php?id=fr');
      }
    });
  });
  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }
  
  $("#t_horarios").hide();
  $("#titulo_t").hide();

  $('#pista').change (function () {
    if ($('#pista').val() > 0) {
      $("#t_horarios").show();
      $("#titulo_t").show();
      mostrarHorario();
    }
    else {
      $("#t_horarios").hide();
      $("#titulo_t").hide();
    }
  });

  function mostrarHorario() {
    var pista = $('#pista').val();
    //limpiamos el horario
    $("tr.warning").remove();
    $("tr.info").remove();

    $.ajax({
      url: "./include/ajax/reservas.php",
      type: "POST",
      data: {
        funcion: "comprueba_horario",
        id: pista
      },
      success: function (datos) {
        var data = JSON.parse(datos);
        if (data[0].estado == 'OK') {

          $.ajax({
            url: "./include/ajax/reservas.php",
            type: "POST",
            data: {
              funcion: "muestra_horario",
              pista: pista
            },
            success: function (datos) {
              var data = JSON.parse(datos);
              if (data[0].estado == 'OK') {
                for (i=0;i<data.length;i++) {
                  if (i%2 == 0) {
                    $("#t_horarios").append("<tr class='info' id='"+data[i].fecha+"'><td>"+data[i].fecha+"</td><td id='9:00' class='"+data[i].reservable1+"'>"+data[i].h1+"</td><td id='10:00' class='"+data[i].reservable2+"'>"+data[i].h2+"</td><td id='11:00' class='"+data[i].reservable3+"'>"+data[i].h3+"</td><td id='12:00' class='"+data[i].reservable4+"'>"+data[i].h4+"</td><td id='13:00' class='"+data[i].reservable5+"'>"+data[i].h5+"</td><td id='17:00' class='"+data[i].reservable6+"'>"+data[i].h6+"</td><td id='18:00' class='"+data[i].reservable7+"'>"+data[i].h7+"</td><td id='19:00' class='"+data[i].reservable8+"'>"+data[i].h8+"</td><td id='20:00' class='"+data[i].reservable9+"'>"+data[i].h9+"</td><td id='21:00' class='"+data[i].reservable10+"'>"+data[i].h10+"</td></tr>");
                  }
                  else {
                    $("#t_horarios").append("<tr class='warning' id='"+data[i].fecha+"'><td>"+data[i].fecha+"</td><td id='9:00' class='"+data[i].reservable1+"'>"+data[i].h1+"</td><td id='10:00' class='"+data[i].reservable2+"'>"+data[i].h2+"</td><td id='11:00' class='"+data[i].reservable3+"'>"+data[i].h3+"</td><td id='12:00' class='"+data[i].reservable4+"'>"+data[i].h4+"</td><td id='13:00' class='"+data[i].reservable5+"'>"+data[i].h5+"</td><td id='17:00' class='"+data[i].reservable6+"'>"+data[i].h6+"</td><td id='18:00' class='"+data[i].reservable7+"'>"+data[i].h7+"</td><td id='19:00' class='"+data[i].reservable8+"'>"+data[i].h8+"</td><td id='20:00' class='"+data[i].reservable9+"'>"+data[i].h9+"</td><td id='21:00' class='"+data[i].reservable10+"'>"+data[i].h10+"</td></tr>");
                  }
                }
                
                $('td.no').addClass('danger');
                if ($('td.no').html() == 'Libre' || $('td.no').html() == 'Free')
                  $('td.no').html("-----");
                $('td.si'). hover (
                  function() {
                    $(this).css({"border":"solid red"});
                  },
                  function() {
                    $(this).css({"border":"solid 1px #ddd"});
                  }
                );
                $('td.si').click(function() {
                  var dia = $(this).parent().attr('id');
                  var hora = $(this).attr('id');
                  var pista = $('#pista').val();
                  $.ajax({
                    url: "./include/ajax/reservas.php",
                    type: "POST",
                    data: {
                      funcion: "comprobar_libre",
                      dia: dia,
                      hora: hora,
                      pista: pista
                    },
                    success: function (datos) {
                      var data = JSON.parse(datos);
                      if (data[0].estado == 'OK') {
                        if (data[0].pista == '') {
                          reservarHora(dia, hora);
                        }
                        else {
                          swal ("Ooops!", id['LABEL_RESERVADA'], "error");
                        }
                      }
                    }
                  });
                });
              }
            }
          })
        }
      }
    });
  }; //mostrarHorario

  function reservarHora(dia, hora) {
    pista = $("#pista").val();
    
    swal({
      title: id['LABEL_SEGURO'],
      text: id['LABEL_RESERVARA']+" "+pista+" "+id['LABEL_RESERVARA2']+" "+dia+" "+id['LABEL_RESERVARA3']+" "+hora+" ",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: id['LABEL_CONFIRMAR'],
      closeOnConfirm: false
    }, function(){
      $.ajax({
        url: "./include/ajax/reservas.php",
        type: "POST",
        data: {
          funcion: "reserva_hora",
          dia: dia,
          hora: hora,
          pista: pista
        },
        success: function (datos) {
          swal(id['LABEL_RESERVADO'], id['LABEL_RESERVADO1']+" "+pista+id['LABEL_RESERVADO2']+" "+dia+id['LABEL_RESERVADO3']+" "+hora+" ", "success");
          $("tr.success").remove();
          $("tr.info").remove();
          mostrarHorario();
        }
      });
    });
  };//reservarHora
};