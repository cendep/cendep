window.onload = function (){
	$("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/editar_perfil.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_perfil.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/editar_perfil.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_perfil.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/editar_perfil.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'editar_perfil.php?id=fr');
      }
    });
  });

  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }

  $('form').submit(function (event) {
    event.preventDefault();
  });

  setTimeout(function(){
	  $('#cambiar_u').validate({
	  	rules: {
	      usuario: {
	      	required: true,
	      	minlength: 6
	      	
	      }
	    },
	    messages: {
	      usuario: {
	      	required: id['LABEL_INTR_USUARIO'],
	      	minlength: id['LABEL_INTR_USUARIO2']
	      }
	    }
	  });

	  $('#cambiar_c').validate({
	  	rules: {
	      password: {
          required: true,
          minlength: 6 
        },
        password2: {
          required: true,
          minlength: 6,
          equalTo: '#contrasena'
        }
	    },
	    messages: {
	    	password: {
	        required: id['LABEL_INTR_CONTRASENA2'],
	        minlength: id['LABEL_INTR_CONTRASENA3']
      	},
      	password2: {
       	 required: id['LABEL_INTR_CONTRASENA2'],
       	 minlength: id['LABEL_INTR_CONTRASENA3'],
       	 equalTo: id['LABEL_INTR_CONTRASENA4']
      	}
      }
	  });

	  $('#cambiar_m').validate({
	  	rules: {
	      correo: {
          required: true,
          email: true
        }
	    },
	    messages: {
	    	correo: {
	        required: id['LABEL_INTR_CORREO'],
	        email: id['LABEL_INTR_CORREO2']
	      }
      }
	  });

	  $('#cambiar_t').validate({
	  	rules: {
	      telf: {
          required: true,
          digits: true,
          minlength: 9,
          maxlength: 9
        }
	    },
	    messages: {
	    	telf: {
          required: id['LABEL_INTR_TELEFONO'],
          minlength: id['LABEL_INTR_TELEFONO2'],
          maxlength: id['LABEL_INTR_TELEFONO2'],
          digits: id['LABEL_INTR_TELEFONO3']
        }
      }
	  });
  }, 1000);

  
	  $('#c_usuario').click (function() {
	  	$.ajax ({
	  		url: './include/ajax/editar_perfil.php',
	  		type: 'POST',
	  		data: {
	  			funcion: 'cambiar_usuario',
	  			usuario: $('#n_usuario').val(),
	  			usu_nuevo: $('#usuario').val()
	  		},
	  		success: function(datos) {
	  			data = JSON.parse(datos);
	  			if (data[0].estado != 'ERROR') {
	  				swal({
	  					title: id['LABEL_CAMBIADO'],
	  					text: id['LABEL_U_CAMBIADO'],
	  					type: 'success'
	  				}, function(){
	  					$(location).attr('href','editar_perfil.php');
	  				});
	  			}
	  			else {
	  				if (data[0].error == "vacio") {
	  					swal (id['LABEL_ERROR'], id['LABEL_U_VACIO'], 'error');
	  				}
	  				else {
	  					swal (id['LABEL_ERROR'], id['LABEL_U_EXISTE'], 'error');
	  				}
	  			}
	  		}
	  	});
	  });
	

	  $('#c_contrasena').click (function() {
	  	$.ajax ({
	  		url: './include/ajax/editar_perfil.php',
	  		type: 'POST',
	  		data: {
	  			funcion: 'cambiar_contrasena',
	  			usuario: $('#n_usuario').val(),
	  			cont_nueva: $('#contrasena').val(),
	  			cont_nueva2: $('#contrasena2').val()
	  		},
	  		success: function(datos) {
	  			data = JSON.parse(datos);
	  			if (data[0].estado != 'ERROR') {
	  				swal({
	  					title: id['LABEL_CAMBIADO'],
	  					text: id['LABEL_C_CAMBIADA'],
	  					type: 'success'
	  				}, function(){
	  					$(location).attr('href','editar_perfil.php');
	  				});
	  			}
	  			else {
	  				if (data[0].error == "vacio") {
	  					swal (id['LABEL_ERROR'], id['LABEL_C_VACIO'], 'error');
	  				}
	  				else {
	  					swal (id['LABEL_ERROR'], id['LABEL_C_NOCOINCIDE'], 'error');
	  				}
	  			}
	  		}
	  	});
	  });

	  $('#baja').click (function() {
	  	swal({
	      title: id['LABEL_SEGURO'],
	      text: id['LABEL_DAR_BAJA']+" ",
	      type: "warning",
	      showCancelButton: true,
	      confirmButtonColor: "#DD6B55",
	      confirmButtonText: id['LABEL_CONFIRMAR'],
	      closeOnConfirm: false
	    }, function(){
	    	$.ajax({
	    		url: './include/ajax/editar_perfil.php',
	    		type: 'POST',
	    		data: {
	    			funcion: 'baja_usuario',
	    			usuario: $('#n_usuario').val()
	    		},
	    		success: function (datos) {
	    			data = JSON.parse(datos);
	    			if (data[0].estado != 'ERROR') {
	    				swal(id['LABEL_DADO_BAJA'], id['LABEL_DADO_BAJA2'], 'success');
	    			}
	    		},
	    		complete: function(datos) {
	    			$(location).attr('href', 'index.php');
	    		}
	    	});
	    });
	  });

	function cambiarCorreo() {
	  $('#c_correo').click (function() {
	  	$.ajax ({
	  		url: './include/ajax/editar_perfil.php',
	  		type: 'POST',
	  		data: {
	  			funcion: 'cambiar_correo',
	  			usuario: $('#n_usuario').val(),
	  			corr_nuevo: $('#correo').val()
	  		},
	  		success: function(datos) {
	  			data = JSON.parse(datos);
	  			if (data[0].estado != 'ERROR') {
	  				swal({
	  					title: id['LABEL_CAMBIADO'],
	  					text: id['LABEL_M_CAMBIADO'],
	  					type: 'success'
	  				}, function(){
	  					$(location).attr('href','editar_perfil.php');
	  				});
	  			}
	  			else {
	  				if (data[0].error == "vacio") {
	  					swal (id['LABEL_ERROR'], id['LABEL_M_VACIO'], 'error');
	  				}
	  				else {
	  					swal (id['LABEL_ERROR'], id['LABEL_M_EXISTE'], 'error');
	  				}
	  			}
	  		}
	  	});
	  });
	}//cambiarCorreo

	function cambiarTelefono() {
	  $('#c_telefono').click (function() {
	  	$.ajax ({
	  		url: './include/ajax/editar_perfil.php',
	  		type: 'POST',
	  		data: {
	  			funcion: 'cambiar_telefono',
	  			usuario: $('#n_usuario').val(),
	  			telefono: $('#telf').val()
	  		},
	  		success: function(datos) {
	  			data = JSON.parse(datos);
	  			if (data[0].estado != 'ERROR') {
	  				swal({
	  					title: id['LABEL_CAMBIADO'],
	  					text: id['LABEL_T_CAMBIADO'],
	  					type: 'success'
	  				}, function(){
	  					$(location).attr('href','editar_perfil.php');
	  				});
	  			}
	  			else {
	  				if (data[0].error == "vacio") {
	  					swal (id['LABEL_ERROR'], id['LABEL_T_VACIO'], 'error');
	  				}
	  			}
	  		}
	  	});
	  });
	}//cambiarTelefono
}