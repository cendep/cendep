window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/login.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'index.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/login.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'index.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/login.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'index.php?id=fr');
      }
    });
  });
};