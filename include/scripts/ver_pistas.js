window.onload = function (){
	$("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/ver_pistas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'ver_pistas.php?id=es');
      }
    });
  });

$("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/ver_pistas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'ver_pistas.php?id=en');
      }
    });
  });

$("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/ver_pistas.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'ver_pistas.php?id=fr');
      }
    });
  });

	$('#pista').change(function (){
		if ($('#pista').val() != 0) {
			$.ajax({
	      url: "include/ajax/ver_pistas.php",
	      type: "POST",
	      data: {
	        funcion: "mostrar_foto",
	        id: $('#pista').val()
	      },
	      success: function (datos) {
	        var data = JSON.parse(datos);
	        if (data[0].estado == "OK") {
	        	$('#imagenPista').attr('src', '');
	        	$('#imagenPista').attr('src', 'include/img/pistas/files/'+data[0].foto);
	        }
	      }
	    });
		}
		else
			$('#imagenPista').attr('src', '');
	});
};