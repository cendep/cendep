window.onload = function (){
  $("#langES").click (function () {
    $.ajax({
      url: "./include/ajax/eliminar_usuario.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'es'
      },
      success: function (datos) {
        $(location).attr('href', 'eliminar_usuario.php?id=es');
      }
    });
  });

  $("#langEN").click (function () {
    $.ajax({
      url: "./include/ajax/eliminar_usuario.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'en'
      },
      success: function (datos) {
        $(location).attr('href', 'eliminar_usuario.php?id=en');
      }
    });
  });

  $("#langFR").click (function () {
    $.ajax({
      url: "./include/ajax/eliminar_usuario.php",
      type: "POST",
      data: {
        funcion: "cambiar_idioma",
        id: 'fr'
      },
      success: function (datos) {
        $(location).attr('href', 'eliminar_usuario.php?id=fr');
      }
    });
  });
  var idioma = window.location.href.split("=");
  var id;
  
  switch (idioma[1]) {
    case 'es':
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'en':
      $.ajax({
        url: "include/lang/en.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    case 'fr': 
      $.ajax({
        url: "include/lang/fr.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      }); 
    break;
    default:
      $.ajax({
        url: "include/lang/es.php",
        type: "GET",
        data: {
          trad: "si"
        },
        success: function (datos) {
          id = JSON.parse(datos);
        }
      });
    break;
  }

  $('#eliminar').click (function() {
    if ($('#user').val() != 0) {
      swal({   
        title: id['LABEL_SEGURO'],
        html: true,
        text: id['LABEL_ELIMINARA']+"<h3 style='color:red'>"+$('#user').val()+"</h3><p style='color:red'>"+id['LABEL_ELIMINARA2'],
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      }, function() {
        $.ajax({
          url: './include/ajax/eliminar_usuario.php',
          type: 'POST',
          data: {
            funcion: 'eliminar_usuario',
            usuario: $('#user').val()
          },
          success: function(datos) {
            data = JSON.parse(datos);
            if (data[0].estado != "ERROR") {
              
              swal({
                title: id['LABEL_ELIMINADO'],
                text: id['LABEL_ELIMINADO2'],
                type: "success",
                closeOnConfirm: false
              }, function() {
                $(location).attr('href', 'eliminar_usuario.php');
              }); 
            }
          }
        });   
      });
    }
  });
}