<?php

include("../../funciones.php");

$conexion = conexion();

$page = isset($_POST['page']) ? $_POST['page'] : 1;
$rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
$sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'name';
$sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
$query = isset($_POST['query']) ? $_POST['query'] : false;
$qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;


$jsonData = array('page'=>$page,'total'=>1,'rows'=>array());

$sql = "SELECT * FROM usuarios WHERE tipo = 'user'";
if ($sql = mysqli_query($conexion, $sql)) {
	while ($row = mysqli_fetch_assoc($sql)) {
		$entry = array('id'=>$row['id_usuario'],
			'cell'=>array(
				'id_usuario'=>"<span>".$row['id_usuario']."</span>",
				'nombre'=>"<span>".$row['nombre']."</span>",
				'correo'=>"<span>".$row['correo']."</span>",
				'telefono'=>"<span>".$row['telefono']."</span>"
			),
		);
		$jsonData['rows'][] = $entry;
	}
	
}

echo json_encode($jsonData);