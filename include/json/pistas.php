<?php

include("../../funciones.php");

$conexion = conexion();

$page = isset($_POST['page']) ? $_POST['page'] : 1;
$rp = isset($_POST['rp']) ? $_POST['rp'] : 10;
$sortname = isset($_POST['sortname']) ? $_POST['sortname'] : 'name';
$sortorder = isset($_POST['sortorder']) ? $_POST['sortorder'] : 'desc';
$query = isset($_POST['query']) ? $_POST['query'] : false;
$qtype = isset($_POST['qtype']) ? $_POST['qtype'] : false;

$fecha = date('Y-m-d');

$start = (($page-1) * $rp);
$limit = " LIMIT $start, $rp";

if($query != '') {
	$busca = " WHERE $qtype LIKE '%".$query."%'";
}
else {
	$busca='';
}

$sql = "SELECT count(*) FROM pistas $busca";
$sql = mysqli_query($conexion, $sql);
while ($row = mysqli_fetch_array($sql)) {
	$total = $row[0];
}

$jsonData = array('page'=>$page,'total'=>$total,'rows'=>array());

$sql = "SELECT * FROM pistas $busca $limit";
if ($sql = mysqli_query($conexion, $sql)) {
	while ($row = mysqli_fetch_assoc($sql)) {
		$entry = array('id'=>$row['id_pista'],
			'cell'=>array(
				'id_pista'=>"<span>".$row['id_pista']."</span>",
				'nom_pista_es'=>"<span>".$row['nom_pista_es']."</span>",
				'nom_pista_en'=>"<span>".$row['nom_pista_en']."</span>",
				'nom_pista_fr'=>"<span>".$row['nom_pista_fr']."</span>",
				'imagen'=>"<img src = 'include/img/pistas/files/thumbnail/".$row['imagen_pista']."' style = 'width: 80px; height: auto' />"
			),
		);
		$jsonData['rows'][] = $entry;
	}
	
}

echo json_encode($jsonData);