<?php
  if(!isset($_SESSION)){
    session_start();
  }

  require("funciones.php");

  $conexion = conexion();

  if (isset($_SESSION['idioma'])) {
    switch ($_SESSION['idioma']) {
      case 'es': include ("include/lang/es.php"); break;
      case 'en': include ("include/lang/en.php"); break;
      case 'fr': include ("include/lang/fr.php"); break;
    }
  }

  if (isset($_SESSION['usuario']) AND $_SESSION['tipo'] == 'user') {
    include ("vistas/menuVista.php");
    include ("vistas/ver_pistasVista.php");
    include ("vistas/pie.php");
  }
  else if (isset($_SESSION['usuario']) AND $_SESSION['tipo'] == 'admin') {
    include ("vistas/menu_adminVista.php");
    include ("vistas/ver_pistasVista.php");
    include ("vistas/pie.php");
  }
  else {
    include ("vistas/noVista.php");
    include ("vistas/pie.php");
  }
?>