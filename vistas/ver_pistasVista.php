<script type="text/javascript" src="include/scripts/cendep.js"></script>
<script type="text/javascript" src="include/scripts/ver_pistas.js"></script>

  <div class="row">
    <div class="col-sm-6 col-sm-offset-3 col-xs-12">
      <br>
    </div><hr>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <label class="centrado"><?=$id['LABEL_SELECCIONE4'] ?></label>
      <select class="center-block" id="pista">
        <option select value="0"><?= $id['LABEL_SEL_PISTA']?></option>
        <?php
        switch ($_SESSION['idioma']) {
          case 'es': $sql = "SELECT * FROM pistas ORDER BY nom_pista_es";
          case 'en': $sql = "SELECT * FROM pistas ORDER BY nom_pista_en";
          case 'fr': $sql = "SELECT * FROM pistas ORDER BY nom_pista_fr";
        }
        
        if ($sql = mysqli_query($conexion, $sql)) {
          while ($row = mysqli_fetch_array($sql)) {
            switch ($_SESSION['idioma']) {
              case 'es': echo "<option select value='" . $row['id_pista'] . "'>" . $row['nom_pista_es'] . "</option>"; break;
              case 'en': echo "<option select value='" . $row['id_pista'] . "'>" . $row['nom_pista_en'] . "</option>"; break;
              case 'fr': echo "<option select value='" . $row['id_pista'] . "'>" . $row['nom_pista_fr'] . "</option>"; break;
            }
          }
        }
        ?>
      </select><br>
      <div class="col-sm-12 col-xs-12">
              <center><img id="imagenPista" class="img-responsive"></img></center>
            </div>
    </div>
  </div>