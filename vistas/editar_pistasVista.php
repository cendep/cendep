<link rel="stylesheet" href="include/styles/jquery.fileupload.css">

<script type="text/javascript" src="include/scripts/editar_pistas.js"></script>
<script type="text/javascript" src="include/scripts/flexigrid.pack.js"></script>
<script type="text/javascript" src="include/scripts/jquery.validate.min.js"></script>

<script type="text/javascript" src="include/jQueryFileUpload/js/vendor/jquery.ui.widget.js"></script>

<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

<script type="text/javascript" src="include/jQueryFileUpload/js/jquery.iframe-transport.js"></script>

<script type="text/javascript" src="include/jQueryFileUpload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="include/jQueryFileUpload/js/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="include/jQueryFileUpload/js/jquery.fileupload-image.js"></script>
<script type="text/javascript" src="include/jQueryFileUpload/js/jquery.fileupload-validate.js"></script>
<script type="text/javascript" src="include/scripts/sube_imagenes.js"></script>

<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
    </div>
  </div><hr>
  <div class="row">
    <div class="col-xs-12">
      <table class='flex' id='flex3' ></table>
    </div>
  </div>
  <div class="modal" id="modalAnadir" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="v-cell">
        <div class="modal-content">
          <div class="modal-header">
            <form id="formAnadir" action="editar_pistas.php" method="post">
              <fieldset class="log-reg">
                <legend><?= $id['LABEL_ANADIR_PISTA'] ?></legend>
                <div class="login">
                  <label for="nom_es"><?= $id['LABEL_NOM_ES']?>: </label><br>
                  <input type="text" name="nom_es" id="nom_es"><br>
                </div>
                <div class="login">
                  <label for="nom_en"><?= $id['LABEL_NOM_EN']?>: </label><br>
                  <input type="text" name="nom_en" id="nom_en"><br>
                </div>
                <div class="login">
                  <label for="nom_fr"><?= $id['LABEL_NOM_FR']?>: </label><br>
                  <input type="text" name="nom_fr" id="nom_fr"><br>
                </div>
                
                <div class="login2">
                  <input id="guardar" type="submit" class= "btn btn-primary btn-lg active" name="guardar" value="<?= $id['LABEL_GUARDAR']?>" />
                  <input id="volver" type="button" class= "btn btn-primary btn-lg active" name="volver" value="<?= $id['LABEL_VOLVER']?>" onclick="$(location).attr('href','editar_pistas.php');"/>
                </div><br>
              </fieldset>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
  <a id='Anadir' data-toggle="modal" href="#modalAnadir" href="javascript:void(0);"></a>
  <div class="modal" id="modalEditar" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="v-cell">
        <div class="modal-content">
          <div class="modal-header">
            <form id="formEditar" action="editar_pistas.php" method="post">
              <fieldset class="log-reg">
                <legend><?= $id['LABEL_ANADIR_PISTA'] ?></legend>
                <div class="login">
                  <label for="e_nom_es"><?= $id['LABEL_NOM_ES']?>: </label><br>
                  <input type="text" name="e_nom_es" id="e_nom_es"><br>
                </div>
                <div class="login">
                  <label for="e_nom_en"><?= $id['LABEL_NOM_EN']?>: </label><br>
                  <input type="text" name="e_nom_en" id="e_nom_en"><br>
                </div>
                <div class="login">
                  <label for="e_nom_fr"><?= $id['LABEL_NOM_FR']?>: </label><br>
                  <input type="text" name="e_nom_fr" id="e_nom_fr"><br>
                </div>
                <div class="login">
                  <span class="btn btn-success fileinput-button">
                    <span><?= $id['LABEL_ANADIR_FOTO']?></span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload" type="file" name="files[]">
                  </span>
                  <br>
                  <br>
                  <!-- The global progress bar -->
                  <div id="progress" class="progress">
                      <div class="progress-bar progress-bar-success"></div>
                  </div>
                  <!-- The container for the uploaded files -->
                  <div id="files" class="files"></div>
                </div>
                <div class="login2">
                  <input id="guardar" type="submit" class= "btn btn-primary btn-lg active" name="guardar" value="<?= $id['LABEL_GUARDAR']?>" />
                  <input id="volver" type="button" class= "btn btn-primary btn-lg active" name="volver" value="<?= $id['LABEL_VOLVER']?>" onclick="$(location).attr('href','editar_pistas.php');"/>
                </div><br>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a id='Editar' data-toggle="modal" href="#modalEditar" href="javascript:void(0);"></a>
  <div class="modal" id="modalSubirFoto" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="v-cell">
        <div class="modal-content">
          <div class="modal-header">
            <form id="formSubirFoto" action="editar_pistas.php" method="post">
              <fieldset class="log-reg">
                <span class="btn btn-success fileinput-button">
                    <span>Añadir foto...</span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload" type="file" name="files[]">
                </span>
                <br>
                <br>
                <!-- The global progress bar -->
                <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
                </div>
                <!-- The container for the uploaded files -->
                <div id="files" class="files"></div>
                <br>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a id='SubirFoto' data-toggle="modal" href="#modalSubirFoto" href="javascript:void(0);"></a>