<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Web de reserva de pistas del Centro Deportivo Trasierra">
    <meta name="author" content="Álvaro López Mohedano">

    <title>Centro Deportivo Trasierra</title>

    <link href="include/styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="include/styles/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="include/styles/cendep.css" rel="stylesheet" type="text/css" />
    <link href="include/styles/flexigrid.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="include/scripts/jquery-3.1.1.min.js"></script> 
    <script type="text/javascript" src="include/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="include/scripts/sweetalert.min.js"></script>
    <script type="text/javascript" src="include/scripts/menu.js"></script>

  </head>
  <body class="fondo">
    <div class="content-page">
      <div class="content">
        <div class="container">
          <div class="row fondoMenu">
            <div class="col-sm-5 col-xs-6">
              <h4 class="text-muted menu-title"><label><?= $id['LABEL_MENU']?></label></h4>
              <p>
                <a href='./reservas.php?id=<?= $_SESSION["idioma"]?>' class='btn btn-primary btn-lg active' target='_self' role='button'><?= $id['LABEL_RESERVAS']?></a>
              </p>
              <p>
                <a href='./anulaciones.php?id=<?= $_SESSION["idioma"]?>' class='btn btn-primary btn-lg active' target='_self' role='button'><?= $id['LABEL_ANULACIONES']?></a>
              </p>
              <p>
                <a href='./ver_pistas.php?id=<?= $_SESSION['idioma']?>' class='btn btn-primary btn-lg active' target='_self' role='button'><?= $id['LABEL_VERPISTAS']?></a>
              </p>
              <p>
                <a href='./editar_perfil.php?id=<?= $_SESSION['idioma']?>' class='btn btn-primary btn-lg active' target='_self' role='button'><?= $id['LABEL_EDITAR_PERFIL']?></a>
              </p>
              <p>
                <a href='./index.php' class='btn btn-primary btn-lg active' target='_self' role='button'><?= $id['LABEL_SALIR']?></a>
              </p>
            </div>
            <div class="col-sm-5 col-xs-6">
              <h4 class="text-muted menu-title"><label><?= $id['LABEL_MENU_ADMIN']?> </label></h4>
              <p>
                <a href='./editar_reservas.php?id=<?= $_SESSION["idioma"]?>' class='btn btn-warning btn-lg active' target='_self' role='button'><?= $id['LABEL_EDIT_RESERVAS']?></a>
              </p>

              <p>
                <a href='./editar_pistas.php?id=<?= $_SESSION['idioma']?>' class='btn btn-warning btn-lg active' target='_self' role='button'><?= $id['LABEL_EDITAR_PISTAS']?></a>
              </p>
              <p>
                <a href='./editar_usuarios.php?id=<?= $_SESSION['idioma']?>' class='btn btn-warning btn-lg active' target='_self' role='button'><?= $id['LABEL_EDITAR_USUARIOS']?></a>
              </p>
              <p>
                <a href='./eliminar_usuario.php?id=<?= $_SESSION['idioma']?>' class='btn btn-warning btn-lg active' target='_self' role='button'><?= $id['LABEL_ELIMINAR_USUARIO']?></a>
              </p>
            </div>
            <div class="col-sm-2 col-xs-12">
              <h4 class="text-muted menu-title"><label><?= $id['LABEL_IDIOMAS']?></label></h4>
              <?php
                include 'cabecera_id.php';
              ?>
            </div>
          </div>