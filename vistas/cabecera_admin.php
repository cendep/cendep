<?php
  echo "
  <p>
    <a href='./reservas.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_RESERVAS']."</a>
  </p>
  <p>
    <a href='./editar_reservas.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_EDIT_RESERVAS']."</a>
  </p>
  <p>
    <a href='./anulaciones.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_ANULACIONES']."</a>
  </p>
  <p>
    <a href='./ver_pistas.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_VERPISTAS']."</a>
  </p>
  <p>
    <a href='./editar_pistas.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_EDITAR_PISTAS']."</a>
  </p>
  <p>
    <a href='./editar_perfil.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_EDITAR_PERFIL']."</a>
  </p>
  <p>
    <a href='./editar_usuarios.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_EDITAR_USUARIOS']."</a>
  </p>
  <p>
    <a href='./eliminar_usuario.php?id=".$_SESSION['idioma']."' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_ELIMINAR_USUARIO']."</a>
  </p>
  <p>
    <a href='./index.php?' class='btn btn-primary btn-lg active' target='_self' role='button'>".$id['LABEL_SALIR']."</a>
  </p>
  ";
?>