<script type="text/javascript" src="include/scripts/jquery.validate.min.js"></script>
<script type="text/javascript" src="include/scripts/editar_perfil.js"></script>
	<div class="row">
		<?php
			if (isset($_SESSION['usuario'])){
				echo "<input type='hidden' id='n_usuario' value='".$_SESSION['usuario']."' />";
			}
			else
				echo "<input type='hidden' id='n_usuario' />";
		?>
	</div><hr>
	<div class="row">	
    <div class="col-sm-12">
      <form id="formREditar" action="editar_perfil.php" method="post">
      	<fieldset class="edit_perfil">
      		<legend><?= $id['LABEL_EDITAR'] ?></legend>
            <div class="login">
            	<form id="cambiar_u" action="editar_perfil.php" method="post">
            		<table><tr><td>
		              <label for="usuario"><?= $id['LABEL_USUARIO']?>: </label><br>
		              <input type="text" name="usuario" id="usuario"> 
		            </td><td class="tabla_reg">
		            	<input id="c_usuario" type="submit" class= "btn btn-primary btn-lg active" value="<?= $id['LABEL_CAMBIAR']?>" /><br>
		            </td></tr></table>
	            </form>
            </div><hr>
            <div class="login">
            	<form id="cambiar_c" action="editar_perfil.php" method="post">
            		<table><tr><td>
		              <label for="password"><?= $id['LABEL_CONTRASENA']?>: </label><br>
		              <input type="password" name="password" id="contrasena"><br><br>
		              <label for="password2"><?= $id['LABEL_CONTRASENA2']?>: </label><br>
		              <input type="password" name="password2" id="contrasena2">
	              </td><td class="tabla_reg">
	              	<input id="c_contrasena" type="submit" class= "btn btn-primary btn-lg active" value="<?= $id['LABEL_CAMBIAR']?>" />
	              </td></tr></table>
              </form>
            </div><hr>
            <div class="login">
							<form id="cambiar_m" action="editar_perfil.php" method="post">
								<table><tr><td>
		              <label for="correo"><?= $id['LABEL_CORREO']?>: </label><br>
		              <input type="text" name="correo" id="correo">
								</td><td class="tabla_reg">
		              <input id="c_correo" type="submit" class= "btn btn-primary btn-lg active" value="<?= $id['LABEL_CAMBIAR']?>" /><br>
		            </td></tr></table>
              </form>
            </div><hr>
            <div class="login">
							<form id="cambiar_t" action="editar_perfil.php" method="post">
								<table><tr><td>
		              <label for="telf"><?= $id['LABEL_TELEFONO']?>: </label><br>
		              <input type="text" name="telf" id="telf">
		            </td><td class="tabla_reg">
		            	<input id="c_telefono" type="submit" class= "btn btn-primary btn-lg active" value="<?= $id['LABEL_CAMBIAR']?>" /><br>
								</td></tr></table>
              </form>
            </div><hr>
            <div class="login">
              <input id="baja" type="submit" class= "btn btn-danger btn-lg active" value="<?= $id['LABEL_BAJA']?>" />
              <input id="volver" type="button" class= "btn btn-primary btn-lg active" value="<?= $id['LABEL_VOLVER']?>" onclick="<?php if ($_SESSION['tipo'] == 'user') echo"$(location).attr('href','menu.php');"; else if ($_SESSION['tipo'] == 'admin') echo "$(location).attr('href','menu_admin.php');";?>"/>
            </div><br>
      	</fieldset>
      </form>
    </div>
  </div>