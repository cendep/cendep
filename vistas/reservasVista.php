<script type="text/javascript" src="include/scripts/reservas.js"></script>
  <div class="row">
    <div class="col-xs-6 col-xs-offset-3">
      <input type="hidden" id="idioma" />
    </div>
  </div><hr>
  <div class="row">
    <div class="col-xs-12">
      <label class="centrado"><?=$id['LABEL_SELECCIONE'] ?></label>
      <select class="center-block" id="pista">
        <option select value="0"><?= $id['LABEL_SEL_PISTA']?></option>
        <?php
        switch ($_SESSION['idioma']) {
          case 'es': $sql = "SELECT * FROM pistas ORDER BY nom_pista_es";
          case 'en': $sql = "SELECT * FROM pistas ORDER BY nom_pista_en";
          case 'fr': $sql = "SELECT * FROM pistas ORDER BY nom_pista_fr";
        }
        
        if ($sql = mysqli_query($conexion, $sql)) {
          while ($row = mysqli_fetch_array($sql)) {
            switch ($_SESSION['idioma']) {
              case 'es': echo "<option select value='" . $row['id_pista'] . "'>" . $row['nom_pista_es'] . "</option>"; break;
              case 'en': echo "<option select value='" . $row['id_pista'] . "'>" . $row['nom_pista_en'] . "</option>"; break;
              case 'fr': echo "<option select value='" . $row['id_pista'] . "'>" . $row['nom_pista_fr'] . "</option>"; break;
            }
          }
        }
        ?>
      </select><br>
    </div>
  </div>
  
  
  <div class="row">
    <div class="col-xs-12">
      <label class="centrado" id="titulo_t" hidden><?= $id['LABEL_SELECCIONE2']?></label>
      <table class="table table-responsive table-bordered table-responsive" id="t_horarios" hidden>
        <tr>
          <th width='175' class='bg-primary'><?= $id['LABEL_FECHA'] ?></th>
          <th class='bg-primary'>9:00 - 10:00</th>
          <th class='bg-primary'>10:00 - 11:00</th>
          <th class='bg-primary'>11:00 - 12:00</th>
          <th class='bg-primary'>12:00 - 13:00</th>
          <th class='bg-primary'>13:00 - 14:00</th>
          <th class='bg-primary'>17:00 - 18:00</th>
          <th class='bg-primary'>18:00 - 19:00</th>
          <th class='bg-primary'>19:00 - 20:00</th>
          <th class='bg-primary'>20:00 - 21:00</th>
          <th class='bg-primary'>21:00 - 22:00</th>
        </tr>
      </table>
    </div>
  </div>