<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Web de reserva de pistas del Centro Deportivo Trasierra">
    <meta name="author" content="Álvaro López Mohedano">

    <title>Centro Deportivo Trasierra</title>

    <link href="include/styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="include/styles/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="include/styles/cendep.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="include/scripts/jquery-3.1.1.min.js"></script> 
    <script type="text/javascript" src="include/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="include/scripts/sweetalert.min.js"></script>
    <script type="text/javascript" src="include/scripts/menu.js"></script>

  </head>
  <body class="fondo">
    <div class="content-page">
      <div class="content">
        <div class="container">
          <div class="row fondoMenu">
            <div class="col-sm-3 col-xs-12">
              <h4 class="text-muted menu-title"><label><?= $id['LABEL_MENU']?></label></h4>
              <?php
                include 'cabecera.php';
              ?>
            </div>
            <div class="col-sm-6 col-xs-12">
              <center><img class="img-responsive" src="include/img/logo.jpg"></img></center>
            </div>
            <div class="col-sm-3 col-xs-12">
              <h4 class="text-muted menu-title"><label><?= $id['LABEL_IDIOMAS']?></label></h4>
              <?php
                include 'cabecera_id.php';
              ?>
            </div>
          </div>
