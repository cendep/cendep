<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Centro Deportivo Trassierra</title>

	<link href="include/styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="include/styles/cendep.css" rel="stylesheet" type="text/css" />

  <script type="text/javascript" src="include/scripts/jquery-3.1.1.min.js"></script> 
  <script type="text/javascript" src="include/scripts/bootstrap.min.js"></script>
  <script type="text/javascript" src="include/scripts/sweetalert.min.js"></script>
  <script type="text/javascript" src="include/scripts/cendep.js"></script>
</head>
<body class="fondo">
	<div class="content-page">
    <div class="content">
      <div class="container">
      	<div class="row fondoMenu">
      		<div class="col-xs-12">
      			<h2>No tienes permisos para acceder a esta página.</h2>
      			<h3>Si quieres puedes <a href="registro.php">registrarte</a>.</h3>
      		</div>
      	</div>
