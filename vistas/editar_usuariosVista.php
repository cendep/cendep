<script type="text/javascript" src="include/scripts/editar_usuarios.js"></script>
<script type="text/javascript" src="include/scripts/flexigrid.pack.js"></script>
<script type="text/javascript" src="include/scripts/jquery.validate.min.js"></script>

<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
    </div>
  </div><hr>
  <div class="row">
    <div class="col-xs-12">
      <table class='flex' id='flex1' ></table>
    </div>
  </div>
  <div class="modal" id="modalAnadir" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="v-cell">
        <div class="modal-content">
          <div class="modal-header">
            <form id="formAnadir" action="editar_usuarios.php" method="post">
              <fieldset class="log-reg">
                <legend><?= $id['LABEL_ANADIR'] ?></legend>
                <div class="login">
                  <label for="usuario"><?= $id['LABEL_USUARIO']?>: </label><br>
                  <input type="text" name="usuario" id="usuario"><br>
                </div>
                <div class="login">
                  <label for="password"><?= $id['LABEL_CONTRASENA']?>: </label><br>
                  <input type="password" name="password" id="contrasena"><br>
                </div>
                <div class="login">
                  <label for="password2"><?= $id['LABEL_CONTRASENA2']?>: </label><br>
                  <input type="password" name="password2" id="contrasena2"><br>
                </div>
                <div class="login">
                  <label for="tipo"><?= $id['LABEL_TIPO']?>: </label><br>
                  <select name="tipo" id="tipo">
                    <option value="user" selected><?= $id['LABEL_USER']?></option>
                    <option value="admin" ><?= $id['LABEL_ADMIN']?></option>
                  </select><br>
                </div>
                <div class="login">
                  <label for="correo"><?= $id['LABEL_CORREO']?>: </label><br>
                  <input type="text" name="correo" id="correo"><br>
                </div>
                <div class="login">
                  <label for="telf"><?= $id['LABEL_TELEFONO']?>: </label><br>
                  <input type="text" name="telf" id="telf"><br>
                </div>
                <div class="login2">
                  <input id="guardar" type="submit" class= "btn btn-primary btn-lg active" name="guardar" value="<?= $id['LABEL_GUARDAR']?>" />
                  <input id="volver" type="button" class= "btn btn-primary btn-lg active" name="volver" value="<?= $id['LABEL_VOLVER']?>" onclick="$(location).attr('href','editar_usuarios.php');"/>
                </div><br>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a id='Anadir' data-toggle="modal" href="#modalAnadir" href="javascript:void(0);"></a>
    <div class="modal" id="modalEditar" data-backdrop="static">
    <div class="modal-dialog modal-md">
      <div class="v-cell">
        <div class="modal-content">
          <div class="modal-header">
            <form id="formEditar" action="editar_usuarios.php" method="post">
              <fieldset class="log-reg">
                <legend><?= $id['LABEL_EDITAR2'] ?></legend>
                <div class="login">
                  <label for="e_usuario"><?= $id['LABEL_USUARIO']?>: </label><br>
                  <input type="text" name="e_usuario" id="e_usuario"></input><br>
                </div>
                <div class="login">
                  <label for="e_correo"><?= $id['LABEL_CORREO']?>: </label><br>
                  <input type="text" name="e_correo" id="e_correo"><br>
                </div>
                <div class="login">
                  <label for="e_telf"><?= $id['LABEL_TELEFONO']?>: </label><br>
                  <input type="text" name="e_telf" id="e_telf"><br>
                </div>
                <div class="login2">
                  <input id="guardar" type="submit" class= "btn btn-primary btn-lg active" name="guardar" value="<?= $id['LABEL_GUARDAR']?>" />
                  <input id="volver" type="button" class= "btn btn-primary btn-lg active" name="volver" value="<?= $id['LABEL_VOLVER']?>" onclick="$(location).attr('href','editar_usuarios.php');"/>
                </div><br>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <a id='Editar' data-toggle="modal" href="#modalEditar"  href="javascript:void(0);"></a>