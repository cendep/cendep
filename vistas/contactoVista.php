<script type="text/javascript" src="include/scripts/contacto.js"></script>
<script type="text/javascript" src="include/scripts/jquery.validate.min.js"></script>

<hr>
  <div class="row">
    <div class="col-xs-12">
      <form id="formContacto" action="contacto.php" method="post">
        <fieldset class="contacto">
          <legend><?= $id['LABEL_FORM_CONTACTO']?></legend>
          <div class="login">
            <label for="asunto"><?= $id['LABEL_ASUNTO']?>: </label><br>
            <input type="text" name="asunto" id="asunto"><br>
          </div>
          <div class="login">
            <label for="correo"><?= $id['LABEL_CORREO']?>: </label><br>
            <input type="text" name="correo" id="correo"><br>
          </div>
          <div class="login">
            <label for="mensaje"><?= $id['LABEL_MENSAJE']?>: </label><br>
            <textarea rows='6' cols="50" name="mensaje" id="mensaje"></textarea><br>
          </div>
          <div class="login">
            <input id="enviar" type="submit" class= "btn btn-primary btn-lg active" name="enviar" value="<?= $id['LABEL_ENVIAR']?>" />
          </div><br>
        </fieldset>
      </form>
    </div>
  </div>