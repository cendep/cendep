<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Centro Deportivo Trassierra</title>

    <link href="include/styles/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="include/styles/sweetalert.css" rel="stylesheet" type="text/css" />
    <link href="include/styles/cendep.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="include/scripts/jquery-3.1.1.min.js"></script> 
    <script type="text/javascript" src="include/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="include/scripts/jquery.validate.min.js"></script>
    <script type="text/javascript" src="include/scripts/sweetalert.min.js"></script>
    <script type="text/javascript" src="include/scripts/registro.js"></script>
    

  </head>
  <body class="fondo">
    <div class="content-page">
      <div class="content">
        <div class="container">
          <div class="row fondoMenu">
            <div class="col-sm-3 col-xs-12">
            </div>
            <div class="col-sm-6 col-xs-12">
              <center><img class="img-responsive" src="include/img/logo.jpg" class="logo"></img></center><br>
            </div>
            <div class="col-sm-3 col-xs-12">
              <h4 class="text-muted menu-title"><label><?= $id['LABEL_IDIOMAS']?></label></h4>
              <?php
                include ("cabecera_id.php");
              ?>
            </div>
          </div><hr>
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-xs-12">
              <form id="formRegistro" action="registro.php" method="post">
                <fieldset class="log-reg">
                  <legend><?= $id['LABEL_REGISTRO'] ?></legend>
                  <div class="login">
                    <label for="usuario"><?= $id['LABEL_USUARIO']?>: </label><br>
                    <input type="text" name="usuario" id="usuario"><br>
                  </div>
                    <div class="login">
                      <label for="password"><?= $id['LABEL_CONTRASENA']?>: </label><br>
                      <input type="password" name="password" id="contrasena"><br>
                    </div>
                    <div class="login">
                      <label for="password2"><?= $id['LABEL_CONTRASENA2']?>: </label><br>
                      <input type="password" name="password2" id="contrasena2"><br>
                    </div>
                    <div class="login">
                      <label for="correo"><?= $id['LABEL_CORREO']?>: </label><br>
                      <input type="text" name="correo" id="correo"><br>
                    </div>
                    <div class="login">
                      <label for="telf"><?= $id['LABEL_TELEFONO']?>: </label><br>
                      <input type="text" name="telf" id="telf"><br>
                    </div>
                    <div class="login2">
                      <input id="registro" type="submit" class= "btn btn-primary btn-lg active" name="registro" value="<?= $id['LABEL_REGISTRARSE']?>" />
                      <input id="volver" type="button" class= "btn btn-primary btn-lg active" name="volver" value="<?= $id['LABEL_VOLVER']?>" onclick="$(location).attr('href','index.php');"/>
                    </div><br>
                </fieldset>
              </form>
            </div>
          </div>
