<!DOCTYPE html>
<?php
  if(!isset($_SESSION)){
    session_start();
  }

  if (isset($_SESSION['idioma'])) {
    switch ($_SESSION['idioma']) {
      case 'es': include ("include/lang/es.php"); break;
      case 'en': include ("include/lang/en.php"); break;
      case 'fr': include ("include/lang/fr.php"); break;
    }
  }

  if (isset($_SESSION['usuario'])) {
    include ("vistas/menuVista.php");
    include ("vistas/pie.php");
  }
  else {
    include ("vistas/noVista.php");
    include ("vistas/pie.php");
  }
?>