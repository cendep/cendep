<?php

date_default_timezone_set("Europe/Madrid");

function conexion() {
  $db_host = "localhost";
  $db_nombre = "cendep";
  $db_user = "root";
  $db_pass = "";

  $conexion = mysqli_connect('p:' . $db_host, $db_user, $db_pass, $db_nombre);

  $conexion->query("SET NAMES = 'utf8");
  $conexion->query("SET time_zone = 'Europe/Madrid'");
  
  return $conexion;
}
?>